from environment import url
import unittest
from pyunitreport import HTMLTestRunner
from pages.header_navigation import HeaderNavigation
from selenium.common.exceptions import WebDriverException
import allure


class TestSessions(unittest.TestCase):

    def test_validation_on_home_section(self):
        try:
            header_navigation = HeaderNavigation()
            header_navigation.navigate_to_url(url)
            header_navigation.click_on_home()
        except WebDriverException:
            allure.attach('Click on HOME Section', allure.attachment_type.TEXT)

    def test_validation_on_tecnologia_section(self):
        try:
            header_navigation = HeaderNavigation()
            header_navigation.navigate_to_url(url)
            header_navigation.click_on_tecnologia()
        except WebDriverException:
            allure.attach('Click on TECNOLOGIA Section', allure.attachment_type.TEXT)

    def test_validation_on_cultura_section(self):
        try:
            header_navigation = HeaderNavigation()
            header_navigation.navigate_to_url(url)
            header_navigation.click_on_culturadigital()
        except WebDriverException:
            allure.attach('Click on CULTURA Section', allure.attachment_type.TEXT)

    def test_validation_on_startups_section(self):
        try:
            header_navigation = HeaderNavigation()
            header_navigation.navigate_to_url(url)
            header_navigation.click_on_startups()
        except WebDriverException:
            allure.attach('Click on STARTUPS Section', allure.attachment_type.TEXT)

    def test_validation_on_empresas_section(self):
        try:
            header_navigation = HeaderNavigation()
            header_navigation.navigate_to_url(url)
            header_navigation.click_on_empresas()
        except WebDriverException:
            allure.attach('Click on Empresas Section', allure.attachment_type.TEXT)

    def test_validation_on_dev_section(self):
        try:
            header_navigation = HeaderNavigation()
            header_navigation.navigate_to_url(url)
            header_navigation.click_on_dev()
        except WebDriverException:
            allure.attach('Click on Dev Section', allure.attachment_type.TEXT)

    @classmethod
    def tearDown(self):
        # cierra el navegador
        header_navigation = HeaderNavigation()
        header_navigation.quit_browser()

#if __name__ == '__main__':
#    unittest.main(verbosity=2)

if __name__ == "__main__":
    unittest.main(verbosity=5, testRunner=HTMLTestRunner(output='', report_name='test_sections', ))