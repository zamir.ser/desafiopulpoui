from pages.base_page import BasePage
from pages.header_navigation import HeaderNavigation


class PrincipalPage(BasePage):

    def __init__(self):
        BasePage.__init__(self)
        self.header_navigation = HeaderNavigation()
