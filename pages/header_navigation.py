import webbrowser

import allure
import time

from selenium.common import ElementNotVisibleException, ElementNotSelectableException
from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import WebDriverException


class HeaderNavigation(BasePage):

    #selectors of navigation header from page
    sel_home_page_xpath = "//*[@id='header']/div/div/div/div[1]/div/h1/a/img"
    sel_smartphones_page_xpath = "//*[@id='menu-item-497715']/a"
    sel_tecnologia_page_xpath = "//*[@id='menu-item-497712']/a"
    sel_culturadigital_page_xpath = "//*[@id='menu-item-497713']/a"
    sel_startups_page_xpath = "//*[@id='menu-item-511252']/a"
    sel_empresas_page_xpath = "//*[@id='menu-item-497718']/a"
    sel_dev_page_xpath = "//*[@id='menu-item-497719']/a"

    def home(self):
        return self.browser.find_element(By.XPATH, self.sel_home_page_xpath)

    def smartphones(self):
        return self.browser.find_element(By.XPATH, self.sel_smartphones_page_xpath)

    def tecnologia(self):
        element = self.browser.find_element(By.XPATH, self.sel_tecnologia_page_xpath)
        return element

    def culturadigital(self):
        return self.browser.find_element(By.XPATH, self.sel_culturadigital_page_xpath)

    def startups(self):
        return self.browser.find_element(By.XPATH, self.sel_startups_page_xpath)

    def empresas(self):
        return self.browser.find_element(By.XPATH, self.sel_empresas_page_xpath)

    def dev(self):
        return self.browser.find_element(By.XPATH, self.sel_dev_page_xpath)

    def is_left_navigation_displayed(self):
        return self.home().is_displayed() and self.smartphones().is_displayed() and self.tecnologia().is_displayed() and self.culturadigital().is_displayed() and self.startups().is_displayed() and self.empresas().is_displayed() and self.dev().is_displayed()

    def click_on_home(self):
        try:
            time.sleep(2)
            self.home().click()
            allure.attach('Click on Logo Enter.co', allure.attachment_type.TEXT)
        except WebDriverException:
            allure.attach('Error Webbbbbb', allure.attachment_type.TEXT)


    def click_on_smartphones(self):
        time.sleep(2)
        self.smartphones().click()
        allure.attach('Click on SMARTPHONES Section', allure.attachment_type.TEXT)

    def click_on_tecnologia(self):
        wait = WebDriverWait(self.browser, timeout=10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])
        wait.until(EC.element_to_be_clickable((By.XPATH, self.sel_tecnologia_page_xpath)))
        self.tecnologia().click()
        allure.attach('Click on TECNOLOGIA Section', allure.attachment_type.TEXT)

    def click_on_culturadigital(self):
        time.sleep(2)
        self.culturadigital().click()
        allure.attach('Click on CULTURA DIGITAL Section', allure.attachment_type.TEXT)

    def click_on_startups(self):
        time.sleep(2)
        self.startups().click()
        allure.attach('Click on STARTUPS Section', allure.attachment_type.TEXT)

    def click_on_empresas(self):
        time.sleep(2)
        self.empresas().click()
        allure.attach('Click on EMPRESAS Section', allure.attachment_type.TEXT)

    def click_on_dev(self):
        time.sleep(2)
        self.dev().click()
        allure.attach('Click on DEV Section', allure.attachment_type.TEXT)
