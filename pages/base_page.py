from selenium.webdriver.support.wait import WebDriverWait
from pages.browser_manager import BrowserManager
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import allure
from selenium import webdriver

class BasePage(object):

    def __init__(self):
        self.browser = self.get_browser()
        self.browser.maximize_window()

    @property
    def is_loaded(self):
        WebDriverWait(self.browser, 10).until(EC.presence_of_element_located(By.ID, 'header'))
        return True

    @allure.step("Navigate to URL:")
    def navigate_to_url(self, url):
        self.browser.get(url)
        allure.attach(str(f'URL={url}'), f'URL={url}', allure.attachment_type.TEXT)

    def quit_browser(self):
        BrowserManager().quit_browser()

    def get_browser(self):
        return BrowserManager().getBrowser()
