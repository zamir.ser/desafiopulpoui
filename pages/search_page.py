import allure
from selenium.webdriver.common.by import By
from pages.base_page import BasePage
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys

class SearchModulePage(BasePage):

    sel_search_icon_xpath = '//*[@id="header"]/div/div/div/div[2]/div/ul/li/span/i'
    sel_search_textbox_name = 's'

    def __init__(self):
        BasePage.__init__(self)
        self.button_search = (By.XPATH, self.sel_search_icon_xpath)
        self.textbox_search = (By.NAME, self.sel_search_textbox_name)

    def search_button(self):
        return self.browser.find_element(By.XPATH, self.sel_search_icon_xpath)

    def search_textbox(self):
        return self.browser.find_element(By.NAME, self.sel_search_textbox_name)

    def click_on_search_icon(self):
        self.search_button().click()
        allure.attach('Click on Icon Search', allure.attachment_type.TEXT)

    def write_on_search_textbox(self, keyword):
        print("en writhe on search textbox")
        print(keyword)
        time.sleep(2)
        self.search_textbox().send_keys(keyword)
        allure.attach('Input Text on Search TextBox', allure.attachment_type.TEXT)
        time.sleep(5)

    def enter_keyword_on_textbox(self):
        print("en enter_keyword_on_textbox")
        self.search_textbox().send_keys(Keys.ENTER)
        allure.attach('Send Key ENTER on Search TextBox', allure.attachment_type.TEXT)
        time.sleep(100)