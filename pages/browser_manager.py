from selenium import webdriver


class BrowserManager(object):
    browser = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(BrowserManager, cls).__new__(cls)
        return cls.instance

    def getBrowser(self):
        if self.browser is None:
            self.browser = webdriver.Chrome('./chromedriver')
        return self.browser

    def quit_browser(self):
        self.browser.quit()
        self.browser = None
