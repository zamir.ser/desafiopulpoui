----
#Versions
----
1) Python v3.10.1
2) Selenium Web Driver v4.0.0
3) Chrome v103
----
#Import Libraries
----
1) Selenium Web Driver
2) unittest
3) PyUnitReport
4) HTMLTestRunner
5) Allure
-----
#Run Test Cases
-----
1) Run each testCases with command "python [name_test_case]"
2) Press Enter Key
3) Wait while run Test Cases
4) Review Reports on Directory "reports"

=======
