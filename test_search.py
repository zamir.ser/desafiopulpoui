from environment import url
import unittest
from pyunitreport import HTMLTestRunner
from pages.search_page import SearchModulePage


class TestSearchs(unittest.TestCase):

    def test_search_by_keyword(self):
        search_module = SearchModulePage()
        search_module.navigate_to_url(url)

        search_module.click_on_search_icon()
        search_module.write_on_search_textbox('Goku')
        search_module.enter_keyword_on_textbox()


    @classmethod
    def tearDown(self):
        # cierra el navegador
        search_module = SearchModulePage()
        search_module.quit_browser()

#if __name__ == '__main__':
#    unittest.main(verbosity=2)

if __name__ == "__main__":
    unittest.main(verbosity=2, testRunner=HTMLTestRunner(output='', report_name='test_search', ))